using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class activarTxtZona6 : MonoBehaviour
{
    public TMPro.TMP_Text txtZona6;
    public TMPro.TMP_Text txtZona6objetivo;


    private void OnCollisionEnter(Collision collision)
    {
        StartCoroutine(mostrarTxtZona5());
    }

    IEnumerator mostrarTxtZona5()
    {
        txtZona6.enabled = true;
        txtZona6objetivo.enabled = true;
        yield return new WaitForSeconds(2);
        txtZona6.enabled = false;
        txtZona6objetivo.enabled = false;
    }
}
