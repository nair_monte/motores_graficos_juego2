using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ControlJugador : MonoBehaviour
{
    //movimiento
    public float rapidezDesplazamiento;
    


    //salto
    public float magnitudSalto;
    private int contSaltos = 0;
    public int maxSaltos = 1;
    public bool estaEnElPiso = true;



    public Rigidbody rb;
    public CapsuleCollider collider;
    public GameObject jugador;
    
    
    //balas
    public int contador_balas=0;
    public TMPro.TMP_Text txtRecargando;
    private bool apuntando;
    public GameObject fogonazo;
    public GameObject spawnFogonazo;

    //linterna
    public GameObject spawn_luz_linterna;
    public Light luz_linterna;
    private bool luz_on;

    //camaras
    public Camera camaraPrimerPersona;
    //public Transform CamaraApuntar;
    //public Transform camaraJugador;
    //public float velocidadCamara = 200f;


    //enemigos
    public GameObject enemigo;
    public GameObject zonaAparicionEnemigos;
    public GameObject zonaAparicionEnemigos2;
    public GameObject enemigoMov;

    public GameObject zonaAparicionOjo1;
    public GameObject zonaAparicionOjo2;
    private int cont_enemigos;
    private bool ojo1;


    //agacharse
   private bool agachado;

    //jefe
    public GameObject jefe;
    private int cont_hits_jefe2;
    //win
    public GameObject spawnWin;
    public GameObject win;


    //spawnZona3
    public GameObject spawnZona3;


    //zona 6
    public GameObject puertaz6;

    //contador guardianes zona 5
    private int cont_hit_guards;


    //spawnPortal
    public GameObject spawnPortal;

    //interfaz
        //cont_balas
        public TMPro.TMP_Text txtMunicion;
        private int municiones;

    
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;

        rb = GetComponent<Rigidbody>();
        collider = GetComponent<CapsuleCollider>();
        jugador = GameObject.Find("Jugador");
        cont_enemigos = 0;
        agachado = false;
        ojo1 = true;

        cont_hit_guards = 0;
        cont_hits_jefe2 = 0;
        municiones = 30;

        luz_on = false;
        spawn_luz_linterna.SetActive(false);

        puertaz6.SetActive(false);


    }


    void Update()
    {
     
        //movimiento
        float movAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movIzqDer = Input.GetAxis("Horizontal") * rapidezDesplazamiento;
       
       
        movIzqDer *= Time.deltaTime;
        movAdelanteAtras *= Time.deltaTime;


        transform.Translate(movIzqDer, 0, movAdelanteAtras);


        //linterna
        if (Input.GetKeyDown(KeyCode.L))
        {
            luz_on = !luz_on;
            
            if (luz_on)
            {
                spawn_luz_linterna.SetActive(true);
            }

            else if (!luz_on)
            {
                spawn_luz_linterna.SetActive(false);
            }
            
        }
        //salto
        if (Input.GetKeyDown(KeyCode.Space) && (estaEnElPiso || (contSaltos < maxSaltos)))


        {

            rb.velocity = new Vector3(0f, magnitudSalto, 0f * Time.deltaTime);

            estaEnElPiso = false;
            contSaltos++;

        }


        //agacharse
        if (Input.GetKeyDown(KeyCode.LeftControl))

        {
            agachado = !agachado;
            if (agachado)
            {
                transform.localScale = new Vector3(1, 0.5f, 1);
            }
            else if (!agachado)
            {
                transform.localScale = new Vector3(1, 1f, 1);
            }
        }
       
        

        //cursor
        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }


        //if (Input.GetMouseButtonDown(1))
        //{
        //    cam.position = Vector3.Lerp(cam.position, CamaraApuntar.position, velocidadCamara * Time.deltaTime);
        //    cam.rotation = Quaternion.Lerp(cam.rotation, CamaraApuntar.rotation, velocidadCamara * Time.deltaTime);
        //}
        //else
        //{
        //    cam.position = Vector3.Lerp(cam.position, camaraJugador.position, velocidadCamara * Time.deltaTime);
        //    cam.rotation = Quaternion.Lerp(cam.rotation, camaraJugador.rotation, velocidadCamara * Time.deltaTime);
        //}


        //disparo
        if (Input.GetMouseButtonDown(0))
        {
            StartCoroutine(DisparoAutomatico());
        }
        



        //interfaz municiones
        txtMunicion.text = municiones.ToString()+ "/-";


    }

    private void OnCollisionEnter(Collision collision)
    {



        if (collision.gameObject.tag == "Piso")
        {
            estaEnElPiso = true;
            contSaltos = 0;
        }


        if (collision.gameObject.tag == "Enemigos")
        {
            
            Time.timeScale = 0;
        }

        if (collision.gameObject.tag == "Lava")
        {
            GestorDeAudio.instancia.ReproducirSonido("da�o");
            transform.position=spawnZona3.transform.position;
        }





    }

    IEnumerator DisparoAutomatico()
    {
        if (contador_balas < 30)
        {
            Ray ray = camaraPrimerPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            RaycastHit hit;

            //sonido bala
            GestorDeAudio.instancia.ReproducirSonido("disparo");

            //particula fogonazo
            GameObject particulas = Instantiate(fogonazo, spawnFogonazo.transform.position, camaraPrimerPersona.transform.rotation) as GameObject;
            Destroy(particulas, 0.3f);

            contador_balas++;
            municiones--;

            if ((Physics.Raycast(ray, out hit) == true))
            {


                if (hit.collider.name.Substring(0, 7) == "Enemigo")
                {
                    GameObject objetoTocado = GameObject.Find(hit.transform.name);
                    ControlEnemigo scriptObjetoTocado = (ControlEnemigo)objetoTocado.GetComponent(typeof(ControlEnemigo));
                    if (scriptObjetoTocado != null)
                    {
                        scriptObjetoTocado.recibirDa�o();
                        cont_enemigos++;

                        if (cont_enemigos>=0 &&cont_enemigos < 20)
                        {
                            GameObject OtroEnemigo = Instantiate(enemigo, zonaAparicionEnemigos.transform.position + new Vector3(Random.Range(-5f, 5f), Random.Range(-5f, 5f), 0), Quaternion.identity);
                            OtroEnemigo.name = "Enemigo";
                        }

                        if (cont_enemigos >= 20 && cont_enemigos < 40)
                        {
                            GameObject OtroEnemigo = Instantiate(enemigoMov, zonaAparicionEnemigos2.transform.position + new Vector3(Random.Range(-5f, 5f), Random.Range(-5f, 5f), 0), Quaternion.identity);
                            OtroEnemigo.name = "Enemigo";
                            StartCoroutine(CrearOtroEnemigo());

                        }

                        if (cont_enemigos == 40)
                        {

                            GestorDeAudio.instancia.ReproducirSonido("risa");

                        }
                        if (cont_enemigos >= 40 && cont_enemigos < 60)
                        {
                            if (ojo1)
                            {
                                StartCoroutine(Ojo1());
                            }

                            if (!ojo1)
                            {
                                StartCoroutine(Ojo2());
                            }


                        }
                        if (cont_enemigos == 50)
                        {
                            //muere jefe
                            GestorDeAudio.instancia.ReproducirSonido("muerte");
                            Destroy(GameObject.Find("jefe"));


                            transform.position = spawnPortal.transform.position;


                        }

                       


                    }


                    if (hit.collider.tag == "Guardian")
                    {
                        cont_hit_guards++;

                        if (cont_hit_guards == 10)
                        {
                            Destroy(GameObject.Find("Pared4"));
                        }
                    }

                    if (hit.collider.tag == "Jefe2")
                    {
                        cont_hits_jefe2++;

                        if (cont_hits_jefe2==15)
                        {
                            puertaz6.SetActive(true);
                        }
                    }
                }


              
              
            }

            //contador_balas++;
            //municiones--;


            //tiempo entre balas
            yield return new WaitForSeconds(0.2f);
            //volver a disparar

            
        }
       
        
        //recargar a las 30
        if (contador_balas == 30)
        {
            txtRecargando.enabled = true;          
            yield return new WaitForSeconds(2.6f);
            txtRecargando.enabled = false;
            contador_balas = 0;
            municiones = 30;
        }
        
       
    }

     IEnumerator Agacharse(bool agachado)
    {
        if (agachado)
        {
            transform.localScale = new Vector3(1, 0.5f, 1);
            yield return new WaitForSeconds(0.5f);

            
                StartCoroutine(Agacharse(agachado));
            
            
        }
        else
        { 
            transform.localScale = new Vector3(1, 1, 1);
            StopCoroutine(Agacharse(agachado));
        }

    }
    
    IEnumerator CrearOtroEnemigo()
    {
        yield return new WaitForSeconds(2);

        if (cont_enemigos>= 20 && cont_enemigos < 40)
        { 
             GameObject OtroEnemigo = Instantiate(enemigoMov, zonaAparicionEnemigos2.transform.position + new Vector3(Random.Range(-5f, 5f), Random.Range(-5f, 5f), 0), Quaternion.identity);
            OtroEnemigo.name = "Enemigo";
            StartCoroutine(CrearOtroEnemigo());
        }
    }


    IEnumerator Ojo1()
    {
        yield return new WaitForSeconds(1.2f);
        GameObject OtroEnemigo = Instantiate(enemigo, zonaAparicionOjo1.transform.position, Quaternion.identity);
        OtroEnemigo.name = "Enemigo";
        ojo1 = !ojo1;
    }

    IEnumerator Ojo2()
    {
        yield return new WaitForSeconds(1.2f);
        GameObject OtroEnemigo = Instantiate(enemigo, zonaAparicionOjo2.transform.position, Quaternion.identity);
        OtroEnemigo.name = "Enemigo";
        ojo1 = !ojo1;
    }

    IEnumerator ganar()
    {
        yield return new WaitForSeconds(1f);      
        GestorDeAudio.instancia.ReproducirSonido("ganarsonido");
        GameObject winner = Instantiate(win, spawnWin.transform.position, spawnWin.transform.rotation);
    }

    

}
    


