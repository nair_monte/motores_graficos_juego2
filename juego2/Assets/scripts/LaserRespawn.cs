using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserRespawn : MonoBehaviour
{
    public GameObject jugador;
    public GameObject z6spawn;
    
    private void OnTriggerEnter(Collider other)
    {
        jugador.transform.position = z6spawn.transform.position;
    }
}
