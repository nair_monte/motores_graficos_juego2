using System.Collections;

using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
public class ControlJuego : MonoBehaviour

{
    public static ControlJuego instancia;
    public GameObject jugador;


    //spawn jugador
    public GameObject MainSpawn;

    public float tiempo = 0;
    public TMPro.TMP_Text txtCantidadEnemigos;
    public int TotalMatado;
    public GameObject contenedorEnemigos;
    public TMPro.TMP_Text txtTiempo;


    
    //interfaz
        
    
        //recarga
    public TMPro.TMP_Text txtRecargando;


    //zonas
    public TMPro.TMP_Text txtSpawn;
    

    public TMPro.TMP_Text txtZona1;
    public TMPro.TMP_Text txtZona1objetivo;

    public TMPro.TMP_Text txtZona2;
    public TMPro.TMP_Text txtZona2objetivo;

    public TMPro.TMP_Text txtZona3;
    public TMPro.TMP_Text txtZona3objetivo;

    public TMPro.TMP_Text txtZona4;
    public TMPro.TMP_Text txtZona4objetivo;

    public TMPro.TMP_Text txtZona5;
    public TMPro.TMP_Text txtZona5objetivo;
    
    public TMPro.TMP_Text txtZona6;
    public TMPro.TMP_Text txtZona6objetivo;

    public TMPro.TMP_Text txtFinal;
    public TMPro.TMP_Text txtTiempoFinal;


    public GameObject pisoInvisible;

    private int enemigosAsesinados = 0;
    void Start()
    {
        Time.timeScale = 1;
        
        txtRecargando.enabled = false;
        txtZona1.enabled = false;
        txtZona1objetivo.enabled = false;
        txtZona2.enabled = false;
        txtZona2objetivo.enabled = false;
        txtZona3.enabled = false;
        txtZona3objetivo.enabled = false;
        txtZona4.enabled = false;
        txtZona4objetivo.enabled = false;
        txtZona5.enabled = false;
        txtZona5objetivo.enabled = false;
        txtZona6.enabled = false;
        txtZona6objetivo.enabled = false;
        txtFinal.enabled = false;
        txtTiempoFinal.enabled = false;

        ComenzarJuego();


        instancia = this;

        TotalMatado = GetComponents<ControlEnemigo>().Length;
        txtCantidadEnemigos.text = $"Puntos: {TotalMatado.ToString("f0")}";
        txtTiempo.text = "Tiempo: " + tiempo.ToString("f0") + " s";


    }

    void Update()

    {
        //cronometro();
        txtTiempo.text =  tiempo.ToString("f0") + " s";


       
        if (Input.GetKeyDown(KeyCode.R))
        {
            reStart();
        }

      
      
          
        }

    
    void ComenzarJuego()

    {
        jugador.transform.position = MainSpawn.transform.position;
        jugador.transform.rotation = new Quaternion(0,180f,0,0);
        GestorDeAudio.instancia.ReproducirSonido("musica");
        tiempo = 0;
        //cronometro();
    }

    public float cronometro(bool cronometro_on)
    {
        if (cronometro_on)
        {
            tiempo += Time.deltaTime;
        }

        return tiempo;
       
    }

    public void EnemigoMuerto()
    {
        TotalMatado++;
        enemigosAsesinados++;
        txtCantidadEnemigos.text = $"Puntos: {TotalMatado.ToString("f0")}";

        if (TotalMatado == 20)
        {
            Destroy(GameObject.Find("Pared1"));
            StartCoroutine(mostrarTxtZona2());
        }

        if (TotalMatado == 40)
        {
            Destroy(GameObject.Find("Pared2"));
            StartCoroutine(mostrarTxtZona3());
        }

        if (TotalMatado == 50)
        {         
            StartCoroutine(mostrarTxtZona4());
        }

        if (TotalMatado == 55)
        {
            Destroy(GameObject.Find("Pared3"));
        }



        }

    public void reStart()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Shooter");
    }


    IEnumerator mostrarTxtZona2()
    {
        txtZona2.enabled = true;
        txtZona2objetivo.enabled = true;
        yield return new WaitForSeconds(2);
        txtZona2.enabled = false;
        txtZona2objetivo.enabled = false;
    }

    IEnumerator mostrarTxtZona3()
    {
        txtZona3.enabled = true;
        txtZona3objetivo.enabled = true;
        yield return new WaitForSeconds(2);
        txtZona3.enabled = false;
        txtZona3objetivo.enabled = false;
    }

    IEnumerator mostrarTxtZona4()
    {
        txtZona4.enabled = true;
        txtZona4objetivo.enabled = true;
        yield return new WaitForSeconds(2);
        txtZona4.enabled = false;
        txtZona4objetivo.enabled = false;
        
     


    }



}

