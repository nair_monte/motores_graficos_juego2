using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPlataforma : MonoBehaviour
{
    //plataformas
    public GameObject plataforma1;
    public GameObject plataforma2;
    public GameObject plataforma3;
    public GameObject plataforma4;
    public GameObject plataforma5;
    public GameObject plataforma6;
    public GameObject plataforma7;
    public GameObject plataforma8;
    public GameObject plataforma9;
    public GameObject plataforma10;


    public void Start()
    {
        StartCoroutine(PlataformasDesincronizadas());


    }








    IEnumerator desactivarPlataforma1()
    {

        yield return new WaitForSeconds(5f);

        plataforma1.SetActive(false);

        yield return new WaitForSeconds(1.5f);

        plataforma1.SetActive(true);


        StartCoroutine(desactivarPlataforma1());
    }

    IEnumerator desactivarPlataforma2()
    {

        yield return new WaitForSeconds(5f);

        plataforma2.SetActive(false);

        yield return new WaitForSeconds(1.5f);

        plataforma2.SetActive(true);


        StartCoroutine(desactivarPlataforma2());
    }

    IEnumerator desactivarPlataforma3()
    {

        yield return new WaitForSeconds(5f);

        plataforma3.SetActive(false);

        yield return new WaitForSeconds(1.5f);

        plataforma3.SetActive(true);


        StartCoroutine(desactivarPlataforma3());
    }

    IEnumerator desactivarPlataforma4()
    {

        yield return new WaitForSeconds(5f);

        plataforma4.SetActive(false);

        yield return new WaitForSeconds(1.5f);

        plataforma4.SetActive(true);


        StartCoroutine(desactivarPlataforma4());
    }

    IEnumerator desactivarPlataforma5()
    {

        yield return new WaitForSeconds(5f);

        plataforma5.SetActive(false);

        yield return new WaitForSeconds(1.5f);

        plataforma5.SetActive(true);


        StartCoroutine(desactivarPlataforma5());
    }

    IEnumerator desactivarPlataforma6()
    {

        yield return new WaitForSeconds(5f);

        plataforma6.SetActive(false);

        yield return new WaitForSeconds(1.5f);

        plataforma6.SetActive(true);


        StartCoroutine(desactivarPlataforma6());
    }

    IEnumerator desactivarPlataforma7()
    {

        yield return new WaitForSeconds(5f);

        plataforma7.SetActive(false);

        yield return new WaitForSeconds(1.5f);

        plataforma7.SetActive(true);


        StartCoroutine(desactivarPlataforma7());
    }

    IEnumerator desactivarPlataforma8()
    {

        yield return new WaitForSeconds(5f);

        plataforma8.SetActive(false);

        yield return new WaitForSeconds(1.5f);

        plataforma8.SetActive(true);


        StartCoroutine(desactivarPlataforma8());
    }

    IEnumerator desactivarPlataforma9()
    {

        yield return new WaitForSeconds(5f);

        plataforma9.SetActive(false);

        yield return new WaitForSeconds(1.5f);

        plataforma9.SetActive(true);


        StartCoroutine(desactivarPlataforma9());
    }

    IEnumerator desactivarPlataforma10()
    {

        yield return new WaitForSeconds(5f);

        plataforma10.SetActive(false);

        yield return new WaitForSeconds(1.5f);

        plataforma10.SetActive(true);


        StartCoroutine(desactivarPlataforma10());
    }

    IEnumerator PlataformasDesincronizadas()
    {
        StartCoroutine(desactivarPlataforma1());
        StartCoroutine(desactivarPlataforma2());
        yield return new WaitForSeconds(1f);
        StartCoroutine(desactivarPlataforma3());
        StartCoroutine(desactivarPlataforma4());
        yield return new WaitForSeconds(1f);
        StartCoroutine(desactivarPlataforma5());
        yield return new WaitForSeconds(1f);
        StartCoroutine(desactivarPlataforma6());
        yield return new WaitForSeconds(1f);
        StartCoroutine(desactivarPlataforma7());
        StartCoroutine(desactivarPlataforma8());
        yield return new WaitForSeconds(1f);
        StartCoroutine(desactivarPlataforma9());
        StartCoroutine(desactivarPlataforma10());

    }
}
