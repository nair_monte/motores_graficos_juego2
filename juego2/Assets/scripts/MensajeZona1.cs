using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class MensajeZona1 : MonoBehaviour
{
    public GameObject activarZona1;


    public TMPro.TMP_Text txtZona1;
    public TMPro.TMP_Text txtZona1objetivo;


    public TMPro.TMP_Text txtSpawn;


    public bool activarCronometro;

    private void Start()
    {
        activarCronometro = false;
    }


    private void Update()
    {

        if (activarCronometro==true)
        {
            ControlJuego.instancia.cronometro(true);
        }

       

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Jugador")
        {
            //cronometro
            activarCronometro = true;


            //interfaz
            txtSpawn.enabled = false;
            StartCoroutine(desactivarTexto());        
           
        }
    }

    IEnumerator desactivarTexto()
    {
        txtZona1.enabled = true;
        txtZona1objetivo.enabled = true;
        yield return new WaitForSeconds(2);
        txtZona1.enabled = false;
        txtZona1objetivo.enabled =false;
    }



}
