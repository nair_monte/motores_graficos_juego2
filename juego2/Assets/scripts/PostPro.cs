using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine;
public class PostPro : MonoBehaviour 
{
    private Bloom bloom = null;
    private LensDistortion lens_distortion = null;
    private ChromaticAberration chromatic = null;
    void Start() 
    { 
        Volume volume = GetComponent<Volume>();
        volume.sharedProfile.TryGet<Bloom>(out bloom);
        
        volume.sharedProfile.TryGet<LensDistortion>(out lens_distortion);
        volume.sharedProfile.TryGet<ChromaticAberration>(out chromatic);
        if (bloom != null) 
        {
            bloom.intensity.value = 1.0f;
           
        }
        if (lens_distortion != null)
        {
            lens_distortion.intensity.value = -0.5f;
        }
        if (chromatic != null)
        {
            chromatic.intensity.value = 0.6f;
        }
    } 
}