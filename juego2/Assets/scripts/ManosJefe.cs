using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManosJefe : MonoBehaviour
{
    //golpe izquierda
    public GameObject mano1;
    public GameObject mano1Golpeando;
    public GameObject mano1Golpe;
    
    //golpe derecha
    public GameObject mano2;
    public GameObject mano2Golpeando;
    public GameObject mano2Golpe;

    //golpe al medio
    public GameObject mano3Golpe;
    public void Start()
    {
        mano1.SetActive(false);
        mano1Golpeando.SetActive(false);
        mano1Golpe.SetActive(false);

        mano2.SetActive(false);
        mano2Golpeando.SetActive(false);
        mano2Golpe.SetActive(false);

        mano3Golpe.SetActive(false);

        StartCoroutine(GolpeManos());
    }

    IEnumerator GolpeManos()
    {
        mano1.SetActive(true);
        mano2.SetActive(true);
        


        //golpe mano1
        yield return new WaitForSeconds(3);
        mano1.SetActive(false);
        mano1Golpeando.SetActive(true);
        yield return new WaitForSeconds(3);
        mano1Golpeando.SetActive(false);
        mano1Golpe.SetActive(true);      
        yield return new WaitForSeconds(3);
        mano1Golpe.SetActive(false);
        mano1.SetActive(true);


        //golpe mano 2
        yield return new WaitForSeconds(3);
        mano2.SetActive(false);
        mano2Golpeando.SetActive(true);
        yield return new WaitForSeconds(3);
        mano2Golpeando.SetActive(false);
        mano2Golpe.SetActive(true);
        yield return new WaitForSeconds(3);
        mano2Golpe.SetActive(false);
        mano2.SetActive(true);

        //golpe mano 3 (golpe al medio)
        yield return new WaitForSeconds(3);
        mano1.SetActive(false);
        mano2.SetActive(false);
        mano1Golpeando.SetActive(true);
        mano2Golpeando.SetActive(true);
        yield return new WaitForSeconds(3);
        mano1Golpeando.SetActive(false);
        mano2Golpeando.SetActive(false);
        mano3Golpe.SetActive(true);
        yield return new WaitForSeconds(3);
        mano3Golpe.SetActive(false);
        StartCoroutine(GolpeManos());

    }
}
