using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JefeSpawn : MonoBehaviour
{
    public GameObject jefe;
    public GameObject jefeSpawn;
    

    public BoxCollider boxcollider;
    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        GameObject Enemigojefe = Instantiate(jefe, new Vector3(jefeSpawn.transform.position.x, jefeSpawn.transform.position.y, jefeSpawn.transform.position.z), new Quaternion());
        
        Destroy(boxcollider);
    }


    
}
