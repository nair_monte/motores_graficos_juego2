using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class activarTxtz4 : MonoBehaviour
{
    public TMPro.TMP_Text txtZona5;
    public TMPro.TMP_Text txtZona5objetivo;


    private void OnCollisionEnter(Collision collision)
    {
        StartCoroutine(mostrarTxtZona5());
    }

    IEnumerator mostrarTxtZona5()
    {
        txtZona5.enabled = true;
        txtZona5objetivo.enabled = true;
        yield return new WaitForSeconds(2);
        txtZona5.enabled = false;
        txtZona5objetivo.enabled = false;
    }
}
