using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCamara : MonoBehaviour
{

    Vector2 mouseMirar;
    Vector2 suavidadMov;
    public float sensibilidad = 0.5f;
    public float suavizado = 2;

    GameObject jugador;
    
    void Start()
    {
        jugador = this.transform.parent.gameObject;
    }

   
    void Update()
    {
        var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

        md = Vector2.Scale(md, new Vector2(sensibilidad * suavizado, sensibilidad * suavizado));

        suavidadMov.x = Mathf.Lerp(suavidadMov.x, md.x, 1f / suavizado);
        suavidadMov.y = Mathf.Lerp(suavidadMov.y, md.y, 1f / suavizado);


        mouseMirar += suavidadMov;
        //camara entre angulo -90 y 90
        mouseMirar.y = Mathf.Clamp(mouseMirar.y, -90, 90);
        //rotart camara arriba abajop
        transform.localRotation = Quaternion.AngleAxis(-mouseMirar.y, Vector3.right);
        //rotar jugador izq der
        jugador.transform.localRotation = Quaternion.AngleAxis(mouseMirar.x,jugador.transform.up);

    }
}
