using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class z5Enemigo : MonoBehaviour
{



    public GameObject jugador;
    private void Start()
    {
        jugador = GameObject.Find("Jugador");
    }
    void Update()
    {
        
        transform.LookAt(jugador.transform);
        transform.Translate(5*Vector3.forward * Time.deltaTime);
    
    
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Jugador")
        {
            jugador.transform.position = new Vector3(5.5f,-3.5f,-221);
        }
    }
}
