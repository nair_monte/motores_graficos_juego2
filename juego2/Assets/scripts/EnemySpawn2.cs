using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn2 : MonoBehaviour
{
    public GameObject z5enemigo;
    public GameObject enemyspawn;
    public GameObject portal;

    public BoxCollider boxcollider;
    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        StartCoroutine(spawnEnemigo());
        Destroy(boxcollider);
    }


    IEnumerator spawnEnemigo()
    {
        GameObject Enemigo1 = Instantiate(z5enemigo, new Vector3(enemyspawn.transform.position.x, enemyspawn.transform.position.y, enemyspawn.transform.position.z),new Quaternion());
        Enemigo1.name = "Enemigoz52";      

        yield return new WaitForSeconds(3f);
        Destroy(portal);
        

    }
}
