using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PantallaFinal : MonoBehaviour
{

    public TMPro.TMP_Text txtFinal;
    public TMPro.TMP_Text txtTiempoFinal;

    public TMPro.TMP_Text txtCronometro;
    private float tiempo_final;
    private void OnTriggerEnter(Collider other)
    {
       tiempo_final= ControlJuego.instancia.cronometro(false);

        txtCronometro.enabled = false;
        txtFinal.enabled = true;
        txtTiempoFinal.enabled = true;

        txtTiempoFinal.text=$"Tu tiempo fue de:{tiempo_final} segundos.";
    }
}
